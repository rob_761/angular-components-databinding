import {
  Component, OnInit, Input, ViewEncapsulation, SimpleChanges, OnChanges, DoCheck,
  AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, OnDestroy, ViewChild, ElementRef, ContentChild
} from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css'],
  encapsulation: ViewEncapsulation.Emulated //default encapsulation
})
export class ServerElementComponent implements OnInit, OnChanges, DoCheck, AfterContentChecked, AfterContentInit,
                                      AfterViewChecked, AfterViewInit, OnDestroy {

  //@Input is a Decorator to allow access by parent components, parent sets value of element
  //'sElement' is the alias of element to outside components
  @Input('sElement') element: {type: string, name: string, content: string};
  @Input() name: string;

  @ViewChild('heading') header: ElementRef;
  @ContentChild('contentParagraph') paragraph: ElementRef

  constructor() {
    console.log('constructor called');
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnChanges called');
    console.log(changes);
  }

  ngOnInit() {
    console.log('ngOnInit called');
    console.log('Paragraph Content: ' + this.paragraph.nativeElement.textContent);
  }

  //called twice if in development mode, gets triggered by promises etc.
  ngDoCheck() {
    console.log('ngDoCheck called');
  }

  ngAfterContentInit(){
    console.log('ngAfterContentInit called');
    console.log('Text Content: ' + this.header.nativeElement.textContent);
    console.log('Paragraph Content: ' + this.paragraph.nativeElement.textContent);
  }

  //called afer each change detection cycle
  ngAfterContentChecked() {
    console.log('ngAfterContentChecked called');
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit called');
    console.log('Text Content: ' + this.header.nativeElement.textContent);
  }

  ngAfterViewChecked() {
    console.log('ngAfterViewChecked called');
  }

  ngOnDestroy() {
    console.log('ngOnDestroy called');
  }
}
